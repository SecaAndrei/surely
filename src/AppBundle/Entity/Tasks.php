<?php

namespace AppBundle\Entity;

/**
 * Created by PhpStorm.
 * User: Seca
 * Date: 3/14/2017
 * Time: 12:02 PM
 */
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity()
 * @ORM\Table(name="tasks")
 */
class Tasks
{

    /**
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\Item", mappedBy="tasks", cascade={"persist"})
     */
    private $items;

    public function __construct()
    {
        $this->items = new ArrayCollection();
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return ArrayCollection
     */
    public function getItems()
    {
        return $this->items;
    }

    public function addItem(Item $item)
    {
        $item->setTasks($this);
        $this->items->add($item);
    }

    public function removeItem(Item $item)
    {
        $this->items->removeElement($item);
    }

}
<?php
/**
 * Created by PhpStorm.
 * User: Seca
 * Date: 3/14/2017
 * Time: 8:42 PM
 */

namespace AppBundle\Form;

use AppBundle\Entity\Item;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ItemApiType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('title', TextType::class, array(
                'label' => false
            ))
            ->add('description', TextType::class, array(
                'label' => false
            ))
            ->add('submit', SubmitType::class, array(
                'label' => 'Save item',
                'attr' => array(
                    'class' => 'btn btn-success'
                )
            ));
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => Item::class,
        ));
    }
}
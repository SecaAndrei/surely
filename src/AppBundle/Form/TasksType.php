<?php

namespace AppBundle\Form;
use AppBundle\Entity\Tasks;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Created by PhpStorm.
 * User: Seca
 * Date: 3/14/2017
 * Time: 12:10 PM
 */
class TasksType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('items', CollectionType::class, array(
                'entry_type' => ItemType::class,
                'label' => null,
                'allow_add' => true,
                'by_reference' => false,
                'allow_delete' => true
            ))
            ->add('submit', SubmitType::class, array(
                'label' => 'Save all',
                'attr' => array(
                    'class' => 'btn btn-success'
                )
            ))
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => Tasks::class,
        ));
    }

}
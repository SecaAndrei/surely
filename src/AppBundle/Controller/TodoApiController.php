<?php
/**
 * Created by PhpStorm.
 * User: Seca
 * Date: 3/14/2017
 * Time: 8:01 PM
 */

namespace AppBundle\Controller;


use AppBundle\Entity\Item;
use AppBundle\Form\ItemApiType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

class TodoApiController extends Controller
{
    public function listAction() {
        $em = $this->getDoctrine()->getManager();

        $items = $em->getRepository('AppBundle:Item')->findAll();

        return $this->render('@App/todo-api/list.html.twig', array(
            'items' => $items
        ));
    }

    public function editAction($id, Request $request) {

        $em = $this->getDoctrine()->getManager();

        $task = $em->getRepository('AppBundle:Tasks')->findOneBy(array('id' => 1));
        $item = $em->getRepository('AppBundle:Item')->findBy(array('id' => $id));

        if(!$item) {
            $items = $em->getRepository('AppBundle:Item')->findAll();
            return $this->redirectToRoute('todo_api_list', array(
                'items' => $items
            ));
        } else {
            $item = $item[0];
        }

        $form = $this->createForm(ItemApiType::class, $item);
        $form->handleRequest($request);

        if($form->isValid()) {
            $item->setTasks($task);
            $em->persist($item);
            $em->flush();
        }

        return $this->render('@App/todo-api/edit.html.twig', array(
            'item' => $item,
            'form' => $form->createView()
        ));


    }

    public function deleteAction($id) {

        $em = $this->getDoctrine()->getManager();

        $item = $em->getRepository('AppBundle:Item')->findBy(array('id' => $id));

        if($item[0]) {
            $em->remove($item[0]);
            $em->flush();
        }

        $items = $em->getRepository('AppBundle:Item')->findAll();
        return $this->redirectToRoute('todo_api_list', array(
            'items' => $items
        ));

    }

    public function addAction(Request $request) {

        $em = $this->getDoctrine()->getManager();

        $task = $em->getRepository('AppBundle:Tasks')->findOneBy(array('id' => 1));

        $item = new Item();


        $form = $this->createForm(ItemApiType::class, $item);
        $form->handleRequest($request);


        if($form->isValid()) {
            $item->setTasks($task);
            $em->persist($item);
            $em->flush();

            $items = $em->getRepository('AppBundle:Item')->findAll();
            return $this->redirectToRoute('todo_api_list', array(
                'items' => $items
            ));

        }

        return $this->render('@App/todo-api/add.html.twig', array(
           'form' => $form->createView()
        ));


    }

}
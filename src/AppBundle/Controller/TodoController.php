<?php
/**
 * Created by PhpStorm.
 * User: Seca
 * Date: 3/14/2017
 * Time: 11:04 AM
 */

namespace AppBundle\Controller;


use AppBundle\Form\TasksType;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

class TodoController extends Controller
{
    public function listAction(Request $request) {

        $em = $this->getDoctrine()->getManager();
        //get the collection with the items
        $task = $em->getRepository('AppBundle:Tasks')->findOneBy(array('id' => '1'));

        if (!$task) {
            throw $this->createNotFoundException('No task found');
        }

        // Create an ArrayCollection of the current Items objects in the database
        $originalItems = new ArrayCollection();

        foreach ($task->getItems() as $item) {
            $originalItems->add($item);
        }

        // create form collection
        $form = $this->createForm(TasksType::class, $task);
        $form->handleRequest($request);

        if ($form->isValid()) {

            // remove the relationship between the items and the Tasks
            foreach ($originalItems as $item) {
                if (false === $task->getItems()->contains($item)) {
                    $em->remove($item);
                }
            }
            $em->persist($task);
            $em->flush();
            // redirect back
            return $this->redirectToRoute('todo_list');
        }

        return $this->render('@App/todo/list.html.twig', array(
            'form' => $form->createView(),
        ));

    }
}
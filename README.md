surley
======

I have created the task in 2 ways.

1) I have a single route call /todo/list

Here I have a collection named tasks, and it is made of many items. Here you have the list of items and you can add, edit or delete an item. After you change what you want you can save all changes with the save button.  

2) I have multiple routes:

/todo-api/list

- here is the list of items. Here you have add element button for adding a new element.
- each item have edit and delete button;

/todo-api/add

- here is the form for adding a new item in the list;

/todo-api/delete/{id}

- when the button delete from /todo-api/list is pressed, the item is deleted based on the id;

/todo-api/edit/{id}

- when the edit button is pressed, it will direct to route edit based on the id of item;



Structure:

In src folder:
1) I have the controllers. The TodoController is for the todo/list route. Here I have the listAction method which is associated to todo/list route.

The TodoApiController has 4 methods.

- listAction method is for listing the items;
- editAction method is for editing an item;
- deleteAction method is for deleting an item;
- addActin method is for adding an item;

2) I have the Entity folder where the Tasks and Item are doctrine classes. The relationship from Task to Item is OneToMany. In each class, I have the columns and getters and setters for each column.  

3) I have the Form folder where I have the TasksType collection form. It use the ItemType form.
I have the ItemApiType which is used on /todo-api/add route.

4) In resources, I have the config where the routes are stored. Also I have the templating twig views.

In web:

1) I have used bootstrap framework.
